clc
clear 
close all
%% Part A
str = 'music.wav';
[Y,FS]=audioread(str);

L = length(Y);
NFFT = 2^nextpow2(L);
M = fft(Y, NFFT)/L;
f = FS/2*linspace(0,1,NFFT/2);


info = audioinfo(str);
% our file has just a chanel;

% voice = audioplayer(Y , FS);
% play(voice);

%% Part B
bar (f , abs(M(1:NFFT/2)));
ylim([0 .004]);

%% Part C

f1 = 1:1:length(M);
H = (f1 < length(M)/3);
Z = M .* H';
f2 = FS/2*linspace(0,1,length(Z));
figure
bar(f2 ,abs(Z));

output = ifft(Z ,'symmetric');
ylim([0 .004]);
output = output * L;
% voice1 = audioplayer(output , FS);
% play(voice1);

figure;
snr(Y)
figure;
snr(output)

%% Part D

output = downsample( output , 3);
L1 = length(output);
NFFT1 = 2^nextpow2(L1);
f1 = FS/2*linspace(0,1,NFFT1/2);
M1 = fft(Y, NFFT1)/L1;
figure
bar (f1 , abs(M1(1:NFFT1/2)));
ylim([0 .01]);
% voice1 = audioplayer(output , 44100/3);
% play(voice1);

%% Part F , G
% kond tar
% voice1 = audioplayer(output , 44100/4);
% play(voice1);

% tond tar
% voice1 = audioplayer(output , 44100);
% play(voice1);

%% Part H
output = upsample(output,3);
L1 = length(output);
NFFT1 = 2^nextpow2(L1);
f1 = FS/2*linspace(0,1,NFFT1/2);
M1 = fft(Y, NFFT1)/L1;
figure
bar (f1 , abs(M1(1:NFFT1/2)));
ylim([0 .004]);
% voice1 = audioplayer(output , 44100);
% play(voice1);

%% Part i j

% d = fdesign.lowpass('N,Fc',10,info.SampleRate/6,info.SampleRate);
% designmethods(d)
% Hd = design(d);
% output = filter(Hd,Y);


%% K
m = [1, 1, 1];
output = conv(output, m);
L = length(output);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
M1 = fft(output, NFFT)/L;
w = FS/2*linspace(0,1,NFFT/2);
figure
bar(w, abs(M1(1:NFFT/2)));
figure
snr(output)
voice1 = audioplayer(output , 44100);
play(voice1);

%% M
audiowrite('music_fs.wav', output, FS);
audiowrite('music_fs3.wav', output, FS/3);