clc
clear
close all
%% Part A
str = input(' enter your files name : ','s');
[Y,FS]=audioread(str);

%% Part C
info = audioinfo(str);
% our file has just a chnel;

%% Part D
voice = audioplayer(Y , FS);
play(voice);
pause(.1);

%% Part E

MYf = figure;

%% Part F , G , H ,I ,J ,K , L ,M

while (isplaying(voice)  && ishandle(MYf) )
    sam = get(voice, 'CurrentSample');
    y = Y(sam - FS * .1 : sam);
    [pxx, w] = pwelch(y);
    
    Edges = 2*pi*[0, 32, 125, 250, 500, 1000, 2000, 4000, 8000, 16000, FS/2]/FS;
    sub_signal = discretize(y, Edges);
    sub_signal = sub_signal + 1;
    
    bar(log (sub_signal));
    
    xlim([0 info.SampleRate/10]);
    ylim([0 3]);
    pause(0.00001);
    
end

stop(voice);