clc 
clear 
close  all
%% Part A
t = linspace( 0 , 1 , 8192);
% t = 0:1:2*pi*8192;
x = sin( 2*pi*2000 * t);
voice = audioplayer(x , 8192);
play(voice);
pause(2);

%% Part B

x1 = sin( 2*pi*4000 * t);
voice1 = audioplayer(x1 , 8192);
play(voice1);
pause(2);
x2 = sin( 2*pi*5000 * t);
voice2 = audioplayer(x2 , 8192);
play(voice2);
pause(2);

%% Part C

L = length(x);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
four = fftshift(fft(x, NFFT)/L);
L = length(x1);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
four1 = fftshift(fft(x1, NFFT)/L);
L = length(x2);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
four2 = fftshift(fft(x2, NFFT)/L);

t = 4096 * linspace(-1 , 1 , 8192);

figure
s(1) = subplot(2, 3, 1);
plot(t, abs(four));
title(s(1), 'ABS(x)');

s(2) = subplot(2, 3, 2);
plot(t, abs(four1));
title(s(2), 'ABS(x1)');

s(3) = subplot(2, 3, 3);
plot(t, abs(four2));
title(s(3), 'ABS(x2)');

s(4) = subplot(2, 3, 4);
plot(t, angle(four));
title(s(4), 'Angle(x)');

s(5) = subplot(2, 3, 5);
plot(t, angle(four1));
title(s(5), 'Angle(x1)');

s(6) = subplot(2, 3, 6);
plot(t, angle(four2));
title(s(6), 'Angle(x2)');